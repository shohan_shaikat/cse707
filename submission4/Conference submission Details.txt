Hello,

The following submission has been created.

Track Name: ICCIT2022

Paper ID: 9

Paper Title: A Comprehensive Study On Matrix Multiplication Performance

Abstract:
Matrix, which is a term for a rectangular grid of
integers, is one of the most used tools in electrical engineering
and computer science. Both data and mathematical equations can
be represented by the integers in a matrix. Computers are being
used in different fields like image processing, Natural language
processing as well as research related to genetic data, and all
of these fields are required to solve linear equations containing
many variables where asking for matrix multiplication is a
common phenomenon. As a result, matrix multiplication can
be viewed as the solution of linear equations involving specific
variables. Multiplying matrices can provide rapid but accurate
approximations of considerably more complex calculations in
many time-critical engineering applications.
Due to a wide range of applicability, having an efficient matrix
multiplication algorithm is very important. In this paper we
will discuss three matrix multiplication algorithms: Strassen
algorithm, Eigen algorithm and NumPy algorithm and will
analyze their time and space complexity using different sizes
of square matrix.
Index Terms—High Performance Computing, Matrix Multiplication

Created on: Mon, 05 Sep 2022 07:22:32 GMT

Last Modified: Mon, 05 Sep 2022 07:22:32 GMT

Authors:
     - moh.shohanur.rahman@g.bracu.ac.bd (Primary)
     - fahmida.zaman.laboni@g.bracu.ac.bd 
     - mofiz.mojib.haider@g.bracu.ac.bd 
     - sayed.bin.amirul.sakin@g.bracu.ac.bd 
     - shah.sufian.noor.mahady@g.bracu.ac.bd 
     - sumaiya.ismail@g.bracu.ac.bd 
     - md.mustakin.alam@g.bracu.ac.bd 
     - humaion.kabir.mehedi@g.bracu.ac.bd 
     - annajiat@gmail.com 

Secondary Subject Areas: Not Entered
Submission Files:     A-Comprehensive-Study-On-Matrix-Multiplication.pdf (196 Kb, Mon, 05 Sep 2022 07:22:18 GMT) 

Submission Questions Response: Not Entered  

Thanks,
CMT team.





Download the CMT app to access submissions and reviews on the move and receive notifications:
https://apps.apple.com/us/app/conference-management-toolkit/id1532488001
https://play.google.com/store/apps/details?id=com.microsoft.research.cmt

To stop receiving conference emails, you can check the 'Do not send me conference email' box from your User Profile.

Microsoft respects your privacy. To learn more, please read our Privacy Statement.

Microsoft Corporation
One Microsoft Way
Redmond, WA 98052
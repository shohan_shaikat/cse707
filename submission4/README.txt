Link to unlisted youtube video:
https://www.youtube.com/watch?v=EHcwoi73tlQ


Topic Name: A Comprehensive study on matrix multiplication performance

Conference Link: https://iccit.org.bd/2022/paper-submission-guidelines/
Group Number:
2

Group Members:
Fahmida Zaman Laboni		21266026
Sumaiya Ismail			21266027
Shah Sufian Noor Mahady         21266030
Sara Islam			22166031
Moh. Shohanur Rahman		22166040
Sayed Bin Amirul Sakin		22166049
Mofiz Mojib Haider		22166053

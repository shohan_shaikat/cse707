
Student ID: 22166040
Name: Moh. Shohanur Rahman

1. Course Name: Microservice Foundation
   Issuer: LinkedIn	
   Certificate link: 		https://www.linkedin.com/learning/certificates/cd9b7f12eed7dcbd6b6de6cc65b62b98e8cd5975ada1710d0b87f61d1f9cca22?trk=share_certificate

2. Course Name: Understanding Machine Learning
   Issuer: Datacamp	
   certificate: https://gitlab.com/shohan_shaikat/cse707/-/blob/main/submission6/Certificates/Understanding%20of%20machine%20learning.pdf

3. Course Name: Understanding Cloud Computing
   Issuer: Datacamp	
   certificate: https://gitlab.com/shohan_shaikat/cse707/-/blob/main/submission6/Certificates/Understanding%20Cloud%20Computing.pdf

4. Course Name: Aws Cloud Concepts
   Issuer: Datacamp	
   certificate: https://gitlab.com/shohan_shaikat/cse707/-/blob/main/submission6/Certificates/Aws%20Cloud%20Concepts.pdf
	
5. Course Name: Machine Learning with Tree-Based Models in Python
   Issuer: Datacamp	
   certificate: https://gitlab.com/shohan_shaikat/cse707/-/blob/main/submission6/Certificates/Machine%20Learning%20with%20Tree-Based%20Models.pdf

6. Course Name: Cleaning Data in Python
   Issuer: Datacamp	
   certificate: https://gitlab.com/shohan_shaikat/cse707/-/blob/main/submission6/Certificates/Cleaning%20Data%20in%20Python.pdf

7. Course Name: Introduction to Importing Data in Python
   Issuer: Datacamp	
   certificate: https://gitlab.com/shohan_shaikat/cse707/-/blob/main/submission6/Certificates/Introduction%20to%20Importing%20Data%20in%20Python.pdf

8. Course Name: Introduction to Data Science in Python
   Issuer: Datacamp	
   certificate: https://gitlab.com/shohan_shaikat/cse707/-/blob/main/submission6/Certificates/Introduction%20to%20Data%20Science%20in%20Python.pdf
